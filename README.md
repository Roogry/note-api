# Project Structure
* server.js : Memuat kode untuk membuat, mengonfigurasi, dan menjalankan server HTTP menggunakan Hapi.
* routes.js : Memuat kode konfigurasi routing server seperti menentukan path, method, dan handler yang digunakan.
* handler.js : Memuat seluruh fungsi-fungsi handler yang digunakan pada berkas routes.
* notes.js : Memuat data notes yang disimpan dalam bentuk array objek.

# Step to create project like this
1. npm init --y
1. npm install nodemon --save-dev

## Testing
1. buat file src/server.js
1. dalam package.json, buat scripts untuk menjalankan server.js
1. jalankan 'npm run start' pada terminal

## Init Eslint
> It use check our style code. But you can also use VSCode Extension called ESLint. If you use Extension, you can skip it.
1. npm -g i eslint-cli, to install globally in your computer
1. npm install eslint --save-dev
1. npx eslint --init, to init eslint
1. buat scripts untuk menjalankan lint

## Install Hapi
> help me to create HTTP server
1. npm install @hapi/hapi

## Install NanoID
> help me to create unique string id
1. npm install nanoid

## Setup files
1. setup server.js
1. setup routes.js
1. setup nodes.js
1. setup handler.js

# SOME-ORIGIN POLICY 
> Tidak semua data dapat di ambil dari origin (protokol, host, dan port number) yang berbeda. Perlu adanya mekanisme yang dapat membuat mereka saling berinteraksi. Mekanisme tersebut disebut Cross-origin resource sharing (CORS)


